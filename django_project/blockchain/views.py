from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User

"""SERIALIZER & JSON IMPORTS"""
from rest_framework import generics
from . import models
from . import serializers
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt

# must be deleted
from django.views.generic import (ListView, UpdateView, DeleteView)

from rest_framework.parsers import JSONParser

from .models import Block, Transaction, Input, Output
from .serializers import BlockSerializer, TransactionSerializer, InputSerializer, OutputSerializer

"""DEFAULT VIEWS"""


def home(request):
    return render(request, 'blockchain/home.html')


def about(request):
    return render(request, 'blockchain/about.html', {'title': 'About'})


"""BLOCKS VIEWS"""


class BlocksListView(generics.ListCreateAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Block.objects.all()
    serializer_class = serializers.BlockSerializer

    model = Block
    template_name = 'blockchain/block/block-list.html'
    context_object_name = 'blocks'
    ordering = ['-date_posted']
    paginate_by = 9


class BlockDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Block.objects.all()
    serializer_class = serializers.BlockSerializer

    model = Block
    template_name = 'blockchain/block/block-details.html'


class UserBlockListView(ListView):
    model = Block
    template_name = 'blockchain/user-blocks.html'
    context_object_name = 'blocks'
    paginate_by = 9

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Block.objects.filter(author=user).order_by('-date_posted')


class BlockUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Block
    template_name = 'blockchain/block/block-update.html'
    fields = [
            'title',
            'hash',
            'currency',
            'previous_block',
            'merkle_root',
            'time',
            'nonce',
            'difficulty',
            'n_tx',
            'size',
            'block_index',
            'height',
            'received_time'
    ]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        block = self.get_object()
        if self.request.user == block.author:
            return True
        else:
            return False


class BlockDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Block
    template_name = 'blockchain/block/block-confirm-delete.html'
    success_url = '/'

    def test_func(self):
        block = self.get_object()
        if self.request.user == block.author:
            return True
        return False


"""SERIALIZER BLOCK VIEWS"""


@csrf_exempt
def block_list(request):
    if request.method == 'GET':
        blocks = Block.objects.all()
        serializer = BlockSerializer(blocks, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = BlockSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def block_detail(request, pk):
    try:
        block = Block.objects.get(pk=pk)
    except:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = BlockSerializer(block)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = BlockSerializer(block, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        block.delete()
        return HttpResponse(status=204)


"""TRANSACTIONS VIEW"""


class TransactionsListView(generics.ListCreateAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer

    model = Transaction
    template_name = 'blockchain/transaction/transaction-list.html'
    context_object_name = 'transactions'
    ordering = ['-date_posted']
    paginate_by = 9


class TransactionDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Transaction.objects.all()
    serializer_class = serializers.TransactionSerializer

    model = Transaction
    template_name = 'blockchain/transaction/transaction-details.html'


class UserTransactionListView(ListView):
    model = Transaction
    template_name = 'blockchain/transaction/user-transaction.html'
    context_object_name = 'transactions'
    paginate_by = 9

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Transaction.objects.filter(author=user).order_by('-date_posted')


class TransactionUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Transaction
    template_name = 'blockchain/transaction/transaction-update.html'
    fields = [
            'fee',
            'currency',
            'nonce',
            'block_number',
            'tx_index',
            'time',
            'value',
            'gas',
            'gas_price',
            'id',
            'title',
            'date_posted',
            'hash',
            'block_hash',
            'belonging_to',
            'relayed_by',
            'inputs',
            'outputs'
    ]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        transaction = self.get_object()
        if self.request.user == transaction.author:
            return True
        else:
            return False


class TransactionDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Transaction
    template_name = 'blockchain/transaction/transaction-confirm-delete.html'
    success_url = '/'

    def test_func(self):
        transaction = self.get_object()
        if self.request.user == transaction.author:
            return True
        return False


"""SERIALIZER TRANSACTION VIEWS"""


@csrf_exempt
def transaction_list(request):
    if request.method == 'GET':
        transactions = Transaction.objects.all()
        serializer = TransactionSerializer(transactions, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = TransactionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def transaction_detail(request, pk):
    try:
        transaction = Transaction.objects.get(pk=pk)
    except:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = TransactionSerializer(transaction)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = TransactionSerializer(transaction, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        transaction.delete()
        return HttpResponse(status=204)


"""INPUT VIEWS"""


class InputsListView(generics.ListCreateAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Input.objects.all()
    serializer_class = serializers.InputSerializer

    model = Input

    """ To-Do a template"""


class InputDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Input.objects.all()
    serializer_class = serializers.InputSerializer

    model = Input

    """ To-Do a template"""


"""SERIALIZER INPUT VIEWS"""


@csrf_exempt
def input_list(request):
    if request.method == 'GET':
        inputs = Input.objects.all()
        serializer = InputSerializer(inputs, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = InputSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def input_detail(request, pk):
    try:
        input = Input.objects.get(pk=pk)
    except:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = InputSerializer(input)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = InputSerializer(input, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        input.delete()
        return HttpResponse(status=204)


"""OUTPUT VIEWS"""


class OutputsListView(generics.ListCreateAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Output.objects.all()
    serializer_class = serializers.OutputSerializer

    model = Output

    """ To-Do a template"""


class OutputDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
     A view that returns the count of active users in JSON.
    """
    queryset = models.Output.objects.all()
    serializer_class = serializers.OutputSerializer

    model = Output

    """ To-Do a template"""


"""SERIALIZER OUTPUT VIEWS"""


@csrf_exempt
def output_list(request):
    if request.method == 'GET':
        outputs = Output.objects.all()
        serializer = OutputSerializer(outputs, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = OutputSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def output_detail(request, pk):
    try:
        output = Output.objects.get(pk=pk)
    except:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = OutputSerializer(output)
        return JsonResponse(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = OutputSerializer(output, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        output.delete()
        return HttpResponse(status=204)
