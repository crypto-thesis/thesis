# Generated by Django 2.2.1 on 2019-06-07 11:40

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blockchain', '0003_auto_20190607_0934'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='input',
            name='tx_index',
        ),
        migrations.AddField(
            model_name='input',
            name='tx_hash',
            field=models.TextField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='output',
            name='tx_hash',
            field=models.TextField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='block',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 7, 11, 40, 41, 299071, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='block',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 7, 11, 40, 41, 299161, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='input',
            name='address',
            field=models.TextField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='output',
            name='value',
            field=models.BigIntegerField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='date_posted',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 7, 11, 40, 41, 299657, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='time',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 7, 11, 40, 41, 299771, tzinfo=utc)),
        ),
    ]
